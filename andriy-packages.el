;; Disable stupid signature check which never works on windows
(setq package-check-signature nil)

;; Add MELPA repository
(require 'package)
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("tromey" . "http://tromey.com/elpa/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
;;(add-to-list 'package-archives
;;             '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(package-initialize)

;; List all the required packages we want to install automatically
;; on startup
(defvar required-packages
  '(
    ;; display marks as visual
    visible-mark
    ;; discover emacs
    discover
    ;; auto complete with popup
    auto-complete
    ;; git integration
    ;; video tutorial: https://youtu.be/zobx3T7hGNA
    magit
    ;; smex - enchancement to M-x blank prompt
    smex
    ;; latex
    auctex
    ;; Better undo  
    undo-tree
    ;; colorful parenthesis matching
    rainbow-delimiters
    ;; outline toggling for outline mode (see latex config)
    ;;outline-magic //Add it back once figure out MELPA
    ) "a list of packages to ensure are installed at launch.")

(require 'cl)

;method checks to see if all packages are installed
(defun packages-installed-p ()
  (loop for p in required-packages
	when (not (package-installed-p p)) do (return nil)
	finally (return t)))

; if not all packages installed, check one by one
(unless (packages-installed-p)
  ;check for new packages
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ; install missing
  (dolist (p required-packages)
    (when (not (package-installed-p p))
      (package-install p))))
