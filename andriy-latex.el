;; Proper line wrapping for latex
(add-hook 'latex-mode-hook 'visual-line-mode)

;; Enable spell check
;; 1. Install binaries from: http://aspell.net/win32/
;; 2. Install at least one precompiled dictionary (i.e. English)
;; 3. Add its /bin directory to path.
(setq ispell-program-name "aspell")
(add-hook 'latex-mode-hook 'flyspell-mode)

;; Configure Auctex
;;; Don't show headings in different weird sizes
;;; (setq font-latex-fontify-sectioning 'color)
;;; Automatically enable folding mode
(add-hook 'LaTeX-mode-hook (lambda ()
			     (TeX-fold-mode 1)))
;;; Automatically fold the file
(add-hook 'find-file-hook 'TeX-fold-buffer t)

;; Enable the (C-c =) shortcut for quick navigation
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

;; Enable minor outline mode
(add-hook 'LaTeX-mode-hook 'outline-minor-mode)
;; ... and make it easier to switch outlines with C-tab key
(eval-after-load 'outline
  '(progn
     (require 'outline-magic)
     (define-key outline-minor-mode-map (kbd "<C-tab>") 'outline-cycle)))

