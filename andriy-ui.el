;; Speed up emacs in windows 7 and the like
(setq w32-get-true-file-attributes nil)

;; Show parenthesis
(show-paren-mode 1)

;; Disable emacs beep or sound
(setq visible-bell t)

;; Hide startup help screen
(setq inhibit-startup-message t)

;; Remove UI elements
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
;;(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

;; Show column number
(setq column-number-mode t)

;; Highlight current line
;;;;(global-hl-line-mode t)

;; Show line numbers
(global-linum-mode t)

;; Set font to 15
(set-face-attribute 'default nil :family "Consolas" :height 140)

;; Load custom theme
(load-theme 'adwaita t)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(3 ((shift) . 3))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time


