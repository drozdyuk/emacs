;; How to load this config
;; ------------------------
;; 1. Locate your emacs config (see NOTE below)
;; 2. Include the following 2 lines in it:
;;      (add-to-list 'load-path "~/../../My Projects/emacs")
;;      (load "emacs.el")
;; where the path, of course, points to this directory.
;; 3. DONE
;;
;; Config File Locations
;; --------------------
;; In Windows:
;; C:\Users\drozzy\AppData\Roaming\.emacs
;;
;; In Linux:
;; ~/.emacs

;; Install all our packages
(load "andriy-packages.el")

;; Latex specific settings
;;(load "andriy-latex.el") // Doesn't work order of

;; UI Specific settings
(load "andriy-ui.el")

;; git settings
(load "andriy-git.el")

;; Insert newline if C-n at the end of the buffer (saves Enter)
(setq next-line-add-newlines t)

;; Restore opened files and windows config
;;;(desktop-save-mode 1)

;; Enable emacs discover - learning emacs
(global-discover-mode 1)

;; Show emacs marks
(global-visible-mark-mode 1)

;; Enable wildcard buffer list search
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
;;(setq ido-create-new-buffer 'always)
(ido-mode t) ;; turn it on

;; Enable automatic bracket closure
(electric-pair-mode 1)

;; Enable org-mode
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

;; Haskell mode
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)

;; Stop dired from going buffer crazy
(put 'dired-find-alternate-file 'disabled nil)

;; Store all backup and autosave files in the tmp dir
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Disable creating temporary lock files with hashtag in current directory
(setq create-lockfiles nil)
